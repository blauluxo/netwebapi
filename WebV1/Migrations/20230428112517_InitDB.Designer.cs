﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebV1.Capas.Modelo.Data;

#nullable disable

namespace WebV1.Migrations
{
    [DbContext(typeof(WevApiContext))]
    [Migration("20230428112517_InitDB")]
    partial class InitDB
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("MedicoPaciente", b =>
                {
                    b.Property<int>("IdPaciente")
                        .HasColumnType("int");

                    b.Property<int>("medicoId")
                        .HasColumnType("int");

                    b.HasKey("IdPaciente", "medicoId");

                    b.HasIndex("medicoId");

                    b.ToTable("medicoPaciente", (string)null);
                });

            modelBuilder.Entity("WebV1.Seguridad.Models.Cita", b =>
                {
                    b.Property<int>("idCita")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("ID_CITA");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idCita"));

                    b.Property<int>("atributo11")
                        .HasColumnType("int")
                        .HasColumnName("ATRIBUTO11");

                    b.Property<int>("citaMedicoId")
                        .HasColumnType("int")
                        .HasColumnName("citaMedicoId");

                    b.Property<int>("citaPacienteId")
                        .HasColumnType("int")
                        .HasColumnName("citaUsuarioId");

                    b.Property<DateTime>("fechahora")
                        .HasColumnType("datetime2")
                        .HasColumnName("FECHAHORA");

                    b.Property<string>("motivocita")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("MOTIVOCITA");

                    b.HasKey("idCita");

                    b.ToTable("Cita", (string)null);
                });

            modelBuilder.Entity("WebV1.Seguridad.Models.Diagnostico", b =>
                {
                    b.Property<int>("idDiagnostico")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("ID_DIAGNOSTICO");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idDiagnostico"));

                    b.Property<int>("citaId")
                        .HasColumnType("int")
                        .HasColumnName("CITA_ID");

                    b.Property<string>("enfermedad")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("ENFERMEDAD");

                    b.Property<string>("valoracionespecialista")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("VALORACIONESPECIALISTA");

                    b.HasKey("idDiagnostico");

                    b.HasIndex("citaId")
                        .IsUnique();

                    b.ToTable("Diagnostico", (string)null);
                });

            modelBuilder.Entity("WebV1.Seguridad.Models.Medico", b =>
                {
                    b.Property<int>("IdUSuario")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDUSUARIO");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdUSuario"));

                    b.Property<string>("Apellidos")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("APELLIDOS");

                    b.Property<string>("Clave")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("CLAVE");

                    b.Property<string>("Email")
                        .HasColumnType("varchar(255)")
                        .HasColumnName("EMAIL");

                    b.Property<string>("Nombre")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("NOMBRE");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("PASSWORD");

                    b.Property<int>("medicoId")
                        .HasColumnType("int")
                        .HasColumnName("medicoId");

                    b.Property<string>("numcolegiado")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("NUMCOLEGIADO");

                    b.Property<string>("usuario")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("USUARIO");

                    b.HasKey("IdUSuario");

                    b.ToTable("Medico", (string)null);
                });

            modelBuilder.Entity("WebV1.Seguridad.Models.Paciente", b =>
                {
                    b.Property<int>("IdUSuario")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDUSUARIO");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdUSuario"));

                    b.Property<string>("Apellidos")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("APELLIDOS");

                    b.Property<string>("Clave")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("CLAVE");

                    b.Property<string>("Direccion")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("DIRECCION");

                    b.Property<string>("Email")
                        .HasColumnType("varchar(255)")
                        .HasColumnName("EMAIL");

                    b.Property<int>("IdPaciente")
                        .HasColumnType("int")
                        .HasColumnName("ID_PACIENTE");

                    b.Property<string>("Nombre")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("NOMBRE");

                    b.Property<string>("Nss")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("NSS");

                    b.Property<string>("NumTarjeta")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("NUMTARJETA");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("PASSWORD");

                    b.Property<string>("Telefono")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("TELEFONO");

                    b.Property<string>("usuario")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("USUARIO");

                    b.HasKey("IdUSuario");

                    b.ToTable("Paciente", (string)null);
                });

            modelBuilder.Entity("MedicoPaciente", b =>
                {
                    b.HasOne("WebV1.Seguridad.Models.Paciente", null)
                        .WithMany()
                        .HasForeignKey("IdPaciente")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("WebV1.Seguridad.Models.Medico", null)
                        .WithMany()
                        .HasForeignKey("medicoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("WebV1.Seguridad.Models.Diagnostico", b =>
                {
                    b.HasOne("WebV1.Seguridad.Models.Cita", "cita")
                        .WithOne("diagnostico")
                        .HasForeignKey("WebV1.Seguridad.Models.Diagnostico", "citaId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("cita");
                });

            modelBuilder.Entity("WebV1.Seguridad.Models.Cita", b =>
                {
                    b.Navigation("diagnostico");
                });
#pragma warning restore 612, 618
        }
    }
}
