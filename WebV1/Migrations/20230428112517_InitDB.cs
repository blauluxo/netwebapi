﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebV1.Migrations
{
    /// <inheritdoc />
    public partial class InitDB : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cita",
                columns: table => new
                {
                    ID_CITA = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FECHAHORA = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MOTIVOCITA = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ATRIBUTO11 = table.Column<int>(type: "int", nullable: false),
                    citaUsuarioId = table.Column<int>(type: "int", nullable: false),
                    citaMedicoId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cita", x => x.ID_CITA);
                });

            migrationBuilder.CreateTable(
                name: "Medico",
                columns: table => new
                {
                    IDUSUARIO = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    medicoId = table.Column<int>(type: "int", nullable: false),
                    NUMCOLEGIADO = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NOMBRE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    APELLIDOS = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    USUARIO = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CLAVE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EMAIL = table.Column<string>(type: "varchar(255)", nullable: true),
                    PASSWORD = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medico", x => x.IDUSUARIO);
                });

            migrationBuilder.CreateTable(
                name: "Paciente",
                columns: table => new
                {
                    IDUSUARIO = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ID_PACIENTE = table.Column<int>(type: "int", nullable: false),
                    NSS = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NUMTARJETA = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TELEFONO = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DIRECCION = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NOMBRE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    APELLIDOS = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    USUARIO = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CLAVE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EMAIL = table.Column<string>(type: "varchar(255)", nullable: true),
                    PASSWORD = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paciente", x => x.IDUSUARIO);
                });

            migrationBuilder.CreateTable(
                name: "Diagnostico",
                columns: table => new
                {
                    ID_DIAGNOSTICO = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VALORACIONESPECIALISTA = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ENFERMEDAD = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CITA_ID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diagnostico", x => x.ID_DIAGNOSTICO);
                    table.ForeignKey(
                        name: "FK_Diagnostico_Cita_CITA_ID",
                        column: x => x.CITA_ID,
                        principalTable: "Cita",
                        principalColumn: "ID_CITA",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "medicoPaciente",
                columns: table => new
                {
                    IdPaciente = table.Column<int>(type: "int", nullable: false),
                    medicoId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_medicoPaciente", x => new { x.IdPaciente, x.medicoId });
                    table.ForeignKey(
                        name: "FK_medicoPaciente_Medico_medicoId",
                        column: x => x.medicoId,
                        principalTable: "Medico",
                        principalColumn: "IDUSUARIO",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_medicoPaciente_Paciente_IdPaciente",
                        column: x => x.IdPaciente,
                        principalTable: "Paciente",
                        principalColumn: "IDUSUARIO",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Diagnostico_CITA_ID",
                table: "Diagnostico",
                column: "CITA_ID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_medicoPaciente_medicoId",
                table: "medicoPaciente",
                column: "medicoId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Diagnostico");

            migrationBuilder.DropTable(
                name: "medicoPaciente");

            migrationBuilder.DropTable(
                name: "Cita");

            migrationBuilder.DropTable(
                name: "Medico");

            migrationBuilder.DropTable(
                name: "Paciente");
        }
    }
}
