﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebV1.Capas.Modelo.Entidad
{
    public abstract class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IDUSUARIO")]
        public int IdUSuario { get; set; }

        //[Required]
        [Column("NOMBRE")]
        public string Nombre { get; set; }

        //[Required]
        [Column("APELLIDOS")]
        public string Apellidos { get; set; }

        // [Required]
        [Column("USUARIO")]
        public string usuario { get; set; }

        // [Required]
        [Column("CLAVE")]
        public string Clave { get; set; }


        [Required]
        [Column("EMAIL", TypeName = "varchar(255)")]
        public string Email { get; set; }

        [Required]
        [Column("PASSWORD")]
        public string Password { get; set; }


    }
}


//-------------------------------------COSAS MIAS---------------------------------------------

/*
 [Table("Usuario")]
 [Inheritance("TABLE_PER_CLASS")]
 [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
 [StringLength(50, MinimumLength = 2, ErrorMessage = "First name must be between 2 and 50 characters.")]
 [Index(IsUnique = true)]  --> no puedo decirle que es unico y el º  using System.ComponentModel.DataAnnotations.Schema;
 [Key] // PK
 [DatabaseGenerated(DatabaseGeneratedOption.Identity)] //Esto es para autoincrementar
 una colleccion de pacientes
 public virtual ICollection<Paciente> ListaPacientes { get; set; } = new List<Paciente>();
*/
