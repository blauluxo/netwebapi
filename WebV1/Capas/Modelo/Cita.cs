﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebV1.Seguridad.Models
{
    [Table("Cita")]
    public class Cita
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ID_CITA")]
        public int idCita { get; set; }

        [Column("FECHAHORA")]
        public DateTime fechahora { get; set; }


        [Column("MOTIVOCITA")]
        public String motivocita { get; set; }
        [Column("ATRIBUTO11")]
        public int atributo11 { get; set; }

        [Column("citaUsuarioId")]
        public int citaPacienteId { get; set; }
      //  public virtual Paciente paciente { get; set; }  

        [Column("citaMedicoId")]
        public int citaMedicoId { get; set; }
    //    public virtual Medico medico { get; set; }
     
        public Diagnostico diagnostico;

        
    }
}
