﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebV1.Capas.Modelo.Entidad;

namespace WebV1.Seguridad.Models
{
    [Table("Medico")]
    public class Medico : Usuario
    {
        [Column("medicoId")]
        public int medicoId { get; set; }

        [Column("NUMCOLEGIADO")]
        public string numcolegiado { get; set; }
       
        //---------------RELACION DE MUHCOS A MUCHOS CON PACIENTE-----
        [ForeignKey("medicoId")]
        public virtual ICollection<Paciente> ListaPacientesMTM { get; set; } 


    }
}
/*-----------------------------cosas mias-------------------------------------------------------
 * 
 *  [MaxLength(200, ErrorMessage = "The {0} field can not have more than {1} characters.")]
    [Required(ErrorMessage = "The field {0} is mandatory.")]
 *  public ICollection<Article> Articles { get; set; } (relacion muchos a muchos)
    [Required]
    //[RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
    //[StringLength(50, MinimumLength = 2, ErrorMessage = "First name must be between 2 and 50 characters.")]
    //[Index(IsUnique = true)]  --> no puedo decirle que es unico y el º  using System.ComponentModel.DataAnnotations.Schema;
    [Required]
        [InverseProperty("ListaMedicos")]
        public List<Paciente> ListaPacientes { get; set; }

//-----------------------------------------------------------------------------------------------
 */