﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebV1.Seguridad.Models
{
    [Table("Diagnostico")]

    public class Diagnostico
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ID_DIAGNOSTICO")]
        public int idDiagnostico { get; set; }

        [Column("VALORACIONESPECIALISTA")]
        public String valoracionespecialista { get; set; }

        [Column("ENFERMEDAD")]
        public String enfermedad { get; set; }


        [Column("CITA_ID")]
        public int citaId { get; set; }
        public virtual Cita cita { get; set; }  


    }
}
