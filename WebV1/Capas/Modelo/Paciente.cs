﻿
using System.ComponentModel.DataAnnotations.Schema;

using WebV1.Capas.Modelo.Entidad;
using WebV1.Capas.Modelo;
namespace WebV1.Seguridad.Models
{

    [Table("Paciente")]
    public class Paciente : Usuario
    {

        [Column("ID_PACIENTE")]
        public int IdPaciente { get; set; }

        [Column("NSS")]
        public string Nss { get; set; }

        [Column("NUMTARJETA")]
        public string NumTarjeta { get; set; }

        [Column("TELEFONO")]
        public string Telefono { get; set; }

        [Column("DIRECCION")]
        public string? Direccion { get; set; }


        //-------------RELACION DE MUCHOS A MUCHOS
        [ForeignKey("IdPaciente")]
        public virtual ICollection<Medico> ListaMedicosMTM { get; set; }

    }

}
/*-----------------------------cosas mias------------------------------------------------
 * 
 *   [MaxLength(200, ErrorMessage = "The {0} field can not have more than {1} characters.")]
    [Required(ErrorMessage = "The field {0} is mandatory.")]
 *     public ICollection<Article> Articles { get; set; } (relacion muchos a muchos)
    [Required]
       //[RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        //[StringLength(50, MinimumLength = 2, ErrorMessage = "First name must be between 2 and 50 characters.")]
        //[Index(IsUnique = true)]  --> no puedo decirle que es unico y el º  using System.ComponentModel.DataAnnotations.Schema;
        [Required]

//-----------------------------------------------------------------------------------------------
 */