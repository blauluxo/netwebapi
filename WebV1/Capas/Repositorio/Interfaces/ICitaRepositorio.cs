﻿
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Repositorio.Interfaces
{
    public interface ICitaRepositorio
    {
        Task<IEnumerable<Cita>> ObtenerTodasCitas();              //obtener todos los usuarios
        Task<Cita> AgregarCita(Cita cita);                 //Agregar un usuario
        Task<Cita> EliminarCitaByID(int idcita);               //eliminar un usuario por el id
        Task<Cita> ActualizarCitaByID(int id, Cita cita);  //actualizar un usuario
    
    }

}
