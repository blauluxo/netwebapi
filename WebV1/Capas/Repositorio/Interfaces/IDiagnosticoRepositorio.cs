﻿
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Repositorio.Interfaces
{
    public interface IDiagnosticoRepositorio
    {
        Task<IEnumerable<Diagnostico>> ObtenerTodoDiagnosticos();              //obtener todos los usuarios
        Task<Diagnostico> AgregarDiagnostico(Diagnostico diagnostico);                 //Agregar un usuario
        Task<Diagnostico> EliminarDiagnosticoByID(int idDiagnostico);               //eliminar un usuario por el id
        Task<Diagnostico> ActualizarDiagnosticoByID(int id, Diagnostico diagnostico);  //actualizar un usuario
    
    }

}
