﻿
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Repositorio.Interfaces
{
    public interface IMedicoRepositorio
    {
        Task<IEnumerable<Medico>> ObtenerTodosMedicos();              //obtener todos los usuarios
        Task<Medico> AgregarMedico(Medico medico);                 //Agregar un usuario
        Task<Medico> EliminarMedicoByID(int idMedico);               //eliminar un usuario por el id
        Task<Medico> ActualizarMedicosByID(int id, Medico medico);  //actualizar un usuario
    
    }

}
