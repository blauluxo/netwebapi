﻿
using WebV1.Capas.Dtos;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Repositorio.Interfaces
{
    public interface IPacienteRepositorio
    {
        Task<IEnumerable<Paciente>> ObtenerTodosPaciente();              //obtener todos los usuarios
        Task<Paciente> AgregarPaciente(Paciente paciente);                 //Agregar un usuario
        Task<Paciente> EliminarPacienteByID(int idPaciente);               //eliminar un usuario por el id
        Task<Paciente> ActualizarPacienteByID(int id, Paciente medico);  //actualizar un usuario
    
    }

}
