﻿using Microsoft.EntityFrameworkCore;
using WebV1.Capas.Modelo.Data;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Repositorio.implementacion
{
    public class DiagnosticoRepositorio :IDiagnosticoRepositorio
    {

        private readonly WevApiContext _context;

        public DiagnosticoRepositorio(WevApiContext context)
        {
            _context = context;

        }

        public async Task<Diagnostico> ActualizarDiagnosticoByID(int id, Diagnostico diagnostico)
        {

            //  var pacienteEncontrado = await _context.ListaPacientes.FindAsync(id);
            var diagnosticoEncontrado = await _context.ListaDiagnostico
                                  .Where(p => p.idDiagnostico == id)
                                  .FirstOrDefaultAsync();

            if (diagnosticoEncontrado != null)
            {
                diagnosticoEncontrado.valoracionespecialista = diagnostico.valoracionespecialista;
                diagnosticoEncontrado.enfermedad = diagnostico.enfermedad;

                await _context.SaveChangesAsync();
            }

            return diagnosticoEncontrado;
        }

        public async Task<Diagnostico> AgregarDiagnostico(Diagnostico diagnostico)
        {
            _context.ListaDiagnostico.Add(diagnostico);
            await _context.SaveChangesAsync();
            return diagnostico;
        }

        public async Task<Diagnostico> EliminarDiagnosticoByID(int idDiagnostico)
        {
            var diagnosticoEncontrado = await _context.ListaDiagnostico.FindAsync(idDiagnostico);
            if (diagnosticoEncontrado != null)
            {
                _context.ListaDiagnostico.Remove(diagnosticoEncontrado);
                await _context.SaveChangesAsync();
            }

            return diagnosticoEncontrado;
        }

        public async Task<IEnumerable<Diagnostico>> ObtenerTodoDiagnosticos()
        {
            return await _context.ListaDiagnostico.ToListAsync();
        }
    }
}
