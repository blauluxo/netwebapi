﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebV1.Capas.Dtos;
using WebV1.Capas.Modelo.Data;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Repositorio.implementacion
{
    public class PacienteRepositorio : IPacienteRepositorio
    {

        private readonly WevApiContext _context;
        private readonly IMapper _mapper;

        public PacienteRepositorio(WevApiContext context)
        {
            _context = context;
            
        }
        
        public async Task<Paciente> ActualizarPacienteByID(int id, Paciente paciente)
        {

            var pacienteEncontrado = await _context.ListaPacientes
                                  .Where(p => p.IdUSuario == id)
                                  .FirstOrDefaultAsync();

            if (pacienteEncontrado != null)
            {
                pacienteEncontrado.Nombre = paciente.Nombre;
                pacienteEncontrado.Apellidos = paciente.Apellidos;
                pacienteEncontrado.Email = paciente.Email;
               
                await _context.SaveChangesAsync();
            }

            return pacienteEncontrado;

        }

        public async Task<Paciente> EliminarPacienteByID(int idpaciente)
        {
            var pacienteEncontrado = await _context.ListaPacientes.FindAsync(idpaciente);
            if (pacienteEncontrado != null)
            {
                _context.ListaPacientes.Remove(pacienteEncontrado);
                await _context.SaveChangesAsync();
            }

            return pacienteEncontrado;
        }

        public async Task<IEnumerable<Paciente>> ObtenerTodosPaciente()
        {
            return await _context.ListaPacientes.ToListAsync();
        }

        public  async Task<Paciente> AgregarPaciente(Paciente paciente)
        {
            var maxId = _context.ListaPacientes.Select(p => p.IdPaciente).DefaultIfEmpty().Max();
            paciente.IdPaciente = maxId + 1;

            _context.ListaPacientes.Add(paciente);
            await _context.SaveChangesAsync();
            return paciente;
        }

    
    }

}
