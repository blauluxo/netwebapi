﻿using Microsoft.EntityFrameworkCore;
using WebV1.Capas.Modelo.Data;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Repositorio.implementacion
{
    public class CitaRepositorio : ICitaRepositorio
    {
        private readonly WevApiContext _context;

        public CitaRepositorio(WevApiContext context)
        {
            _context = context;

        }

        public async Task<Cita> ActualizarCitaByID(int id, Cita cita)
        {

            //  var pacienteEncontrado = await _context.ListaPacientes.FindAsync(id);
            var citaEncontrado = await _context.ListaCitas
                                  .Where(p => p.idCita == id)
                                  .FirstOrDefaultAsync();

            if (citaEncontrado != null)
            {
                citaEncontrado.motivocita = cita.motivocita;
                citaEncontrado.atributo11 = cita.atributo11;

                await _context.SaveChangesAsync();
            }

            return citaEncontrado;
        }

        public async Task<Cita> AgregarCita(Cita cita)
        {
            _context.ListaCitas.Add(cita);
            await _context.SaveChangesAsync();
            return cita;
        }

        public async Task<Cita> EliminarCitaByID(int idcita)
        {
            var citaEncontrado = await _context.ListaCitas.FindAsync(idcita);
            if (citaEncontrado != null)
            {
                _context.ListaCitas.Remove(citaEncontrado);
                await _context.SaveChangesAsync();
            }

            return citaEncontrado;
        }

        public async Task<IEnumerable<Cita>> ObtenerTodasCitas()
        {
            return await _context.ListaCitas.ToListAsync();
        }
    }
}
