﻿using Microsoft.EntityFrameworkCore;
using WebV1.Capas.Modelo.Data;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Repositorio.implementacion
{
    public class MedicoRepositorio : IMedicoRepositorio
    {

        private readonly WevApiContext _context;

        public MedicoRepositorio(WevApiContext context)
        {
            _context = context;

        }


        public async Task<Medico> ActualizarMedicosByID(int id, Medico medico)
        {

            //  var pacienteEncontrado = await _context.ListaPacientes.FindAsync(id);
            var mediconcontrado = await _context.ListaMedicos
                                  .Where(p => p.IdUSuario == id)
                                  .FirstOrDefaultAsync();

            if (mediconcontrado != null)
            {
                mediconcontrado.Nombre = medico.Nombre;
                mediconcontrado.Apellidos = medico.Apellidos;
                mediconcontrado.Email = medico.Email;

                await _context.SaveChangesAsync();
            }

            return mediconcontrado;
        }

        public async Task<Medico> AgregarMedico(Medico medico)
        {
            var maxId = _context.ListaMedicos.Select(p => p.medicoId).DefaultIfEmpty().Max();
            medico.medicoId = maxId + 1;

            _context.ListaMedicos.Add(medico);
            await _context.SaveChangesAsync();
            return medico;
        }

        public async Task<Medico> EliminarMedicoByID(int idMedico)
        {
            var medicoEncontrado = await _context.ListaMedicos.FindAsync(idMedico);
            if (medicoEncontrado != null)
            {
                _context.ListaMedicos.Remove(medicoEncontrado);
                await _context.SaveChangesAsync();
            }

            return medicoEncontrado;
        }

        public async Task<IEnumerable<Medico>> ObtenerTodosMedicos()
        {
            return await _context.ListaMedicos.ToListAsync();
        }


    }
}
