﻿using AutoMapper;
using WebV1.Capas.Dtos;
using WebV1.Capas.Repositorio.implementacion;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Servicio.Implementacion
{
    public class MedicoServicio :IMedicoServicio
    {

        private readonly IMedicoRepositorio _medicoRepositorio;
        private readonly IMapper _mapper;

        public MedicoServicio(IMedicoRepositorio medicoRepositorio)
        {
            _medicoRepositorio = medicoRepositorio;

            var mapperConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<Medico, MedicoMostrarDto>();
                cfg.CreateMap<Medico, MedicoAgregarDto>();
                cfg.CreateMap<MedicoAgregarDto, Medico>();
                cfg.CreateMap<MedicoMostrarDto, Medico>();
            });
            _mapper = mapperConfig.CreateMapper();
        }

        public async Task<Medico> ActualizarMedicosByID(int id, MedicoMostrarDto medicoDto)
        {
            var medicoss = _mapper.Map<Medico>(medicoDto);
            return await _medicoRepositorio.ActualizarMedicosByID(id, medicoss);
        }

        public async Task<Medico> AgregarMedico(MedicoAgregarDto medicoDto)
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL
            var medicoss = _mapper.Map<Medico>(medicoDto);
            //encripto
            string passwordHash = BCrypt.Net.BCrypt.HashPassword(medicoss.Password);
            medicoss.Password = passwordHash;

            return await _medicoRepositorio.AgregarMedico(medicoss);
        }

        public async Task<Medico> EliminarMedicoByID(int idMedico)
        {
            return await _medicoRepositorio.EliminarMedicoByID(idMedico);
        }

        public async Task<IEnumerable<MedicoMostrarDto>> ObtenerTodosMedicos()
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL.
            var medicos = await _medicoRepositorio.ObtenerTodosMedicos();

            return _mapper.Map<IEnumerable<MedicoMostrarDto>>(medicos);
        }
    }
}
