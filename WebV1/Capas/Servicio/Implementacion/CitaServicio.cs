﻿using AutoMapper;
using WebV1.Capas.Dtos;
using WebV1.Capas.Repositorio.implementacion;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Servicio.Implementacion
{
    public class CitaServicio :ICitaServicio
    {
        private readonly ICitaRepositorio _citaRepositorio;
        private readonly IMapper _mapper;

        public CitaServicio(ICitaRepositorio citaRepositorio)
        {
            _citaRepositorio = citaRepositorio;

            var mapperConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<Cita, CitaMostrarDto>();
                cfg.CreateMap<Cita, CitaAgregarDto>();
                cfg.CreateMap<CitaAgregarDto, Cita>();
                cfg.CreateMap<CitaMostrarDto, Cita>();
            });
            _mapper = mapperConfig.CreateMapper();
        }

        public async Task<Cita> ActualizarCitaByID(int id, CitaMostrarDto citaDto)
        {
            var citas = _mapper.Map<Cita>(citaDto);
            return await _citaRepositorio.ActualizarCitaByID(id, citas);
        }

        public async Task<Cita> AgregarCita(CitaAgregarDto citaDto)
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL
            var citas = _mapper.Map<Cita>(citaDto);
            return await _citaRepositorio.AgregarCita(citas);
        }

        public async Task<Cita> EliminarCitaByID(int idCita)
        {
            return await _citaRepositorio.EliminarCitaByID(idCita);
        }

        public async Task<IEnumerable<CitaMostrarDto>> ObtenerTodasCitas()
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL.
            var citass = await _citaRepositorio.ObtenerTodasCitas();

            return _mapper.Map<IEnumerable<CitaMostrarDto>>(citass);
        }
    }
}
