﻿using AutoMapper;
using WebV1.Capas.Dtos;
using WebV1.Capas.Repositorio.implementacion;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Servicio.Implementacion
{
    public class DiagnosticoServicio :IDiagnosticoServicio
    {
        private readonly IDiagnosticoRepositorio _diagnosticoRepositorio;
        private readonly IMapper _mapper;

        public DiagnosticoServicio(IDiagnosticoRepositorio diagnosticoRepositorio)
        {
            _diagnosticoRepositorio = diagnosticoRepositorio;

            var mapperConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<Diagnostico, DiagnosticoMostrarDto>();
                cfg.CreateMap<Diagnostico, DiagnosticoAgregarDto>();
                cfg.CreateMap<DiagnosticoAgregarDto, Diagnostico>();
                cfg.CreateMap<DiagnosticoMostrarDto, Diagnostico>();
            });
            _mapper = mapperConfig.CreateMapper();
        }

        public async Task<Diagnostico> ActualizarDiagnosticoByID(int id, DiagnosticoMostrarDto diagnosticoDto)
        {
            var diagnosticos = _mapper.Map<Diagnostico>(diagnosticoDto);
            return await _diagnosticoRepositorio.ActualizarDiagnosticoByID(id, diagnosticos);
        }

        public async Task<Diagnostico> AgregarDiagnostico(DiagnosticoAgregarDto diagnosticoDto)
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL
            var diagnositcos = _mapper.Map<Diagnostico>(diagnosticoDto);
            return await _diagnosticoRepositorio.AgregarDiagnostico(diagnositcos);
        }

        public async Task<Diagnostico> EliminarDiagnosticoByID(int idDiagnostico)
        {
            return await _diagnosticoRepositorio.EliminarDiagnosticoByID(idDiagnostico);
        }

        public async Task<IEnumerable<DiagnosticoMostrarDto>> ObtenerTodoDiagnosticos()
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL.
            var diagnositcos = await _diagnosticoRepositorio.ObtenerTodoDiagnosticos();

            return _mapper.Map<IEnumerable<DiagnosticoMostrarDto>>(diagnositcos);
        }
    }
}
