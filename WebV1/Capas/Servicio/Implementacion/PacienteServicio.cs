﻿using AutoMapper;
using WebV1.Capas.Dtos;
using WebV1.Capas.Repositorio.Interfaces;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Servicio.Implementacion
{
    public class PacienteServicio :IPacienteServicio
    {

        private readonly IPacienteRepositorio _pacienteRepositorio;
        private readonly IMapper _mapper;

        public PacienteServicio(IPacienteRepositorio pacienteRepositorio)
        {
            _pacienteRepositorio = pacienteRepositorio;

            var mapperConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<Paciente, PacienteMostrarDto>();
                cfg.CreateMap<Paciente, PacienteAgregarDto>();
                cfg.CreateMap<PacienteAgregarDto, Paciente>();
                cfg.CreateMap<PacienteMostrarDto, Paciente>();
            });
            _mapper = mapperConfig.CreateMapper();
        }


        public async Task<Paciente> ActualizarPacienteByID(int id, PacienteMostrarDto pacienteDto)
        {
            var paciente = _mapper.Map<Paciente>(pacienteDto);
            return await _pacienteRepositorio.ActualizarPacienteByID(id, paciente);
        }

        public async Task<Paciente> AgregarPaciente(PacienteAgregarDto pacienteDto)
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL
            var paciente = _mapper.Map<Paciente>(pacienteDto);
            //encriptamos
            string passwordHash = BCrypt.Net.BCrypt.HashPassword(paciente.Password);
            paciente.Password = passwordHash;


            return await _pacienteRepositorio.AgregarPaciente(paciente);
        }

        public async Task<Paciente> EliminarPacienteByID(int idMedico)
        {
            return await _pacienteRepositorio.EliminarPacienteByID(idMedico);
        }

        public async Task<IEnumerable<PacienteMostrarDto>> ObtenerTodosPaciente()
        {
            //MAPEAMOS Y DEVOLVEMOS UN PACIENTE NORMAL.
            var pacientes = await _pacienteRepositorio.ObtenerTodosPaciente();

            return _mapper.Map<IEnumerable<PacienteMostrarDto>>(pacientes);
        }
    }
}
