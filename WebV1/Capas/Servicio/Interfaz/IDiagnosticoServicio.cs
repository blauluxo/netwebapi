﻿using WebV1.Capas.Dtos;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Servicio.Interfaz
{
    public interface IDiagnosticoServicio
    {
        Task<IEnumerable<DiagnosticoMostrarDto>> ObtenerTodoDiagnosticos();              //obtener todos los usuarios
        Task<Diagnostico> AgregarDiagnostico(DiagnosticoAgregarDto diagnosticoDto);
        Task<Diagnostico> EliminarDiagnosticoByID(int idDiagnostico);               //eliminar un usuario por el id
        Task<Diagnostico> ActualizarDiagnosticoByID(int id, DiagnosticoMostrarDto diagnostico);  //actualizar un usuario
    }
}
