﻿using WebV1.Capas.Dtos;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Servicio.Interfaz
{
    public interface IPacienteServicio
    {
        Task<IEnumerable<PacienteMostrarDto>> ObtenerTodosPaciente();              //obtener todos los usuarios
        Task<Paciente> AgregarPaciente(PacienteAgregarDto pacienteDto);
        Task<Paciente> EliminarPacienteByID(int idPaciente);               //eliminar un usuario por el id
        Task<Paciente> ActualizarPacienteByID(int id, PacienteMostrarDto paciente);  //actualizar un usuario

    }
}
