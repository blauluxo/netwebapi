﻿using WebV1.Capas.Dtos;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Servicio.Interfaz
{
    public interface ICitaServicio
    {
        Task<IEnumerable<CitaMostrarDto>> ObtenerTodasCitas();              //obtener todos los usuarios
        Task<Cita> AgregarCita(CitaAgregarDto citaDto);
        Task<Cita> EliminarCitaByID(int idCita);               //eliminar un usuario por el id
        Task<Cita> ActualizarCitaByID(int id, CitaMostrarDto cita);  //actualizar un usuario
    
    }
}
