﻿using WebV1.Capas.Dtos;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Servicio.Interfaz
{
    public interface IMedicoServicio
    {
        Task<IEnumerable<MedicoMostrarDto>> ObtenerTodosMedicos();              //obtener todos los usuarios
        Task<Medico> AgregarMedico(MedicoAgregarDto medicoDto);
        Task<Medico> EliminarMedicoByID(int idMedico);               //eliminar un usuario por el id
        Task<Medico> ActualizarMedicosByID(int id, MedicoMostrarDto medico);  //actualizar un usuario
    }
}
