﻿namespace WebV1.Capas.Dtos
{
    public class CitaMostrarDto
    {
        public DateTime fechahora { get; set; }
        public String motivocita { get; set; }
        public int citaMedicoId { get; set; }
        public int citaPacienteId { get; set; }
        public int atributo11 { get; set; }

    }
}
