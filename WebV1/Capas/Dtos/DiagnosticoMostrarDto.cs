﻿using WebV1.Seguridad.Models;

namespace WebV1.Capas.Dtos
{
    public class DiagnosticoMostrarDto
    {
        public String enfermedad { get; set; }
        public String valoracionespecialista { get; set; }
        public int citaId { get; set; }
        public virtual Cita cita { get; set; }

    }
}
