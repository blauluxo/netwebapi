﻿namespace WebV1.Capas.Dtos
{
    public class CitaAgregarDto
    {
        public DateTime fechahora { get; set; }
        public String motivocita { get; set; }
        public int atributo11 { get; set; }
        public int citaMedicoId { get; set; }
        public int citaPacienteId { get; set; }

    }
}
