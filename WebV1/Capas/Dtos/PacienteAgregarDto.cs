﻿namespace WebV1.Seguridad.Servicio.Dtos
{
    public class PacienteAgregarDto
    {
        //de parte del usuario
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }

        public string Password { get; set; }
        public string Clave { get; set; }

        //paciente
        public string Nss { get; set; }
        public string Telefono { get; set; }



    }
}
