﻿namespace WebV1.Capas.Dtos
{
    public class DiagnosticoAgregarDto
    {
        public String enfermedad { get; set; }
        public String valoracionespecialista { get; set; }
        public int citaId { get; set; }

    }
}
