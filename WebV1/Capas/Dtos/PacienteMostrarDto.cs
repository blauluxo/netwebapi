﻿namespace WebV1.Capas.Dtos
{
    public class PacienteMostrarDto
    {
        //de parte del usuario
        public int IdUSuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }

        public string Password { get; set; }
        public string Clave { get; set; }

        //paciente
        public int IdPaciente { get; set; }
        public string Nss { get; set; }
        public string Telefono { get; set; }


    }
}
