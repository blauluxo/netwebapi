﻿using Microsoft.AspNetCore.Mvc;
using WebV1.Capas.Dtos;
using WebV1.Capas.Servicio.Implementacion;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Controlador
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitaControlador : ControllerBase
    {
        private readonly ICitaServicio _citaServicio;
        public CitaControlador(ICitaServicio citaServicio)
        {
            _citaServicio = citaServicio;
        }



        [HttpPost]
        public async Task<ActionResult<Cita>> PostCita(CitaAgregarDto citaDto)
        {
            if (citaDto == null)
            {
                return BadRequest("No se ha podido insertar la cita");
            }

            await _citaServicio.AgregarCita(citaDto);

            return Ok("La cita insertada correctamente");
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CitaMostrarDto>>> ObtenerTodosCitas()
        {
            var citas = await _citaServicio.ObtenerTodasCitas();
            return Ok(citas);
        }

        // DELETE: api/Paciente/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Cita>> DeleteCita(int id)
        {
            var cita = await _citaServicio.EliminarCitaByID(id);
            if (cita == null)
            {
                return NotFound($"No se encontró la cita con el ID {id}");
            }
            return Ok($"La cita con el ID {id} ha sido eliminada");
        }

        // PUT: api/Paciente/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCita(int id, CitaMostrarDto cita)
        {

            var citaExistente = await _citaServicio.ActualizarCitaByID(id, cita);

            if (citaExistente == null)
            {
                return NotFound($"No se encontró la cita con el ID {id}");
            }

            return Ok("La cita con el ID  " + id + " ha sido actualizada");
        }





    }
}
