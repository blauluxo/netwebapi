﻿using Microsoft.AspNetCore.Mvc;
using WebV1.Capas.Dtos;
using WebV1.Capas.Servicio.Implementacion;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;

namespace WebV1.Capas.Controlador
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiagnosticoControlador : ControllerBase
    {
        private readonly IDiagnosticoServicio _diagnosticoServicio;
        public DiagnosticoControlador(IDiagnosticoServicio diagnosticoServicio)
        {
            _diagnosticoServicio = diagnosticoServicio;
        }


        [HttpPost]
        public async Task<ActionResult<Diagnostico>> PostDiagnostico(DiagnosticoAgregarDto diagnosticoDto)
        {
            if (diagnosticoDto == null)
            {
                return BadRequest("No se ha podido insertar la cita");
            }

            await _diagnosticoServicio.AgregarDiagnostico(diagnosticoDto);

            return Ok("El Diagnositoc ha sido insertado correctamente");
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DiagnosticoMostrarDto>>> ObtenerTodosDiagnosticos()
        {
            var diagnostico = await _diagnosticoServicio.ObtenerTodoDiagnosticos();
            return Ok(diagnostico);
        }

        // DELETE: api/Paciente/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Diagnostico>> Deletediagnostico(int id)
        {
            var diagnostico = await _diagnosticoServicio.EliminarDiagnosticoByID(id);
            if (diagnostico == null)
            {
                return NotFound($"No se encontró el diagnostico con el ID {id}");
            }
            return Ok($"El doagnostico con el ID {id} ha sido eliminado");
        }

        // PUT: api/Paciente/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDiagnostico(int id, DiagnosticoMostrarDto diagnostico)
        {

            var diagnosticoExistente = await _diagnosticoServicio.ActualizarDiagnosticoByID(id, diagnostico);

            if (diagnosticoExistente == null)
            {
                return NotFound($"No se encontró el diagnostico con el ID {id}");
            }

            return Ok("El diagnostico con el ID  " + id + " ha sido actualizado");
        }




    }
}
