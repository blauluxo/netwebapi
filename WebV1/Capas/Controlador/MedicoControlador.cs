﻿using Microsoft.AspNetCore.Mvc;
using WebV1.Capas.Dtos;
using WebV1.Capas.Servicio.Implementacion;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Controlador
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicoControlador : ControllerBase
    {
        private readonly IMedicoServicio _medicoServicio;
        public MedicoControlador(IMedicoServicio medicoServicio)
        {
            _medicoServicio = medicoServicio;
        }


        [HttpPost]
        public async Task<ActionResult<Medico>> PostMedico(MedicoAgregarDto medicoDto)
        {
            if (medicoDto == null)
            {
                return BadRequest("Medico No se ha podido insertar");
            }

            await _medicoServicio.AgregarMedico(medicoDto);

            return Ok("Medico insertado correctamente");
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MedicoMostrarDto>>> ObtenerTodosMedicos()
        {
            var medicos = await _medicoServicio.ObtenerTodosMedicos();
            return Ok(medicos);
        }

        // DELETE: api/Paciente/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Medico>> DeleteMedico(int id)
        {
            var medico = await _medicoServicio.EliminarMedicoByID(id);
            if (medico == null)
            {
                return NotFound($"No se encontró el paciente con el ID {id}");
            }
            return Ok($"El paciente con el ID {id} ha sido eliminado");
        }

        // PUT: api/Paciente/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedico(int id, MedicoMostrarDto medico)
        {

            var medicoExistente = await _medicoServicio.ActualizarMedicosByID(id, medico);

            if (medicoExistente == null)
            {
                return NotFound($"No se encontró el paciente con el ID {id}");
            }

            return Ok("El paciente con el ID  " + id + " ha sido actualizado");
        }


    }
}
