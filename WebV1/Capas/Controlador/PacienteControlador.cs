﻿using Microsoft.AspNetCore.Mvc;
using WebV1.Capas.Dtos;
using WebV1.Capas.Servicio.Interfaz;
using WebV1.Seguridad.Models;
using WebV1.Seguridad.Servicio.Dtos;

namespace WebV1.Capas.Controlador
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacienteControlador : ControllerBase
    {

        private readonly IPacienteServicio _pacienteServicio;

        public PacienteControlador(IPacienteServicio pacienteServicio)
        {
            _pacienteServicio = pacienteServicio;
        }

        [HttpPost]
        public async Task<ActionResult<Paciente>> PostPaciente(PacienteAgregarDto pacienteDto)
        {
            if (pacienteDto == null)
            {
                return BadRequest("Paciente No se ha podido insertar");
            }

            await _pacienteServicio.AgregarPaciente(pacienteDto);

            return Ok("Paciente insertado correctamente");
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PacienteMostrarDto>>> ObtenerTodosPacientes()
        {
            var pacientes = await _pacienteServicio.ObtenerTodosPaciente();
            return Ok(pacientes);
        }

        // DELETE: api/Paciente/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Paciente>> DeletePaciente(int id)
        {
            var paciente = await _pacienteServicio.EliminarPacienteByID(id);
            if (paciente == null)
            {
                return NotFound($"No se encontró el paciente con el ID {id}");
            }
            return Ok($"El paciente con el ID {id} ha sido eliminado");
        }

        // PUT: api/Paciente/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPaciente(int id, PacienteMostrarDto paciente)
        {

            var pacienteExistente = await _pacienteServicio.ActualizarPacienteByID(id, paciente);

            if (pacienteExistente == null)
            {
                return NotFound($"No se encontró el paciente con el ID {id}");
            }

            return Ok("El paciente con el ID  " +id+ " ha sido actualizado");
        }

    }
}
