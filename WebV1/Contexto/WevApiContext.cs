﻿using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;
using WebV1.Seguridad.Models;
//herencia tpt, tph, tpc, CCTI 
namespace WebV1.Capas.Modelo.Data
{

    public class WevApiContext : DbContext
    {
        public DbSet<Paciente> ListaPacientes { get; set; }
        public DbSet<Medico> ListaMedicos { get; set; }
        public DbSet<Cita> ListaCitas { get; set; }
        public DbSet<Diagnostico> ListaDiagnostico { get; set; }

        public WevApiContext(DbContextOptions<WevApiContext> options) : base(options)
        {

        }


        //este metodo lo que hace es cambiarte el nombre de la tabla 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ClasePaciente(modelBuilder);

            ClaseMedico(modelBuilder);

            ClaseCita(modelBuilder);
            ClaseDiagnostico(modelBuilder);

              Relaciones(modelBuilder);


        }



        private void Relaciones(ModelBuilder modelBuilder)
        {
            //crea la tabla mucho a muchos es la relacion.
            modelBuilder.Entity<Paciente>()  //Generamos el nombre de la tabla relacion muchos a muchos
                .HasMany(p => p.ListaMedicosMTM)
                .WithMany(m => m.ListaPacientesMTM)
                .UsingEntity(j =>
                {
                    j.ToTable("medicoPaciente");

                });


        }

        private void ClaseDiagnostico(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Diagnostico>().ToTable("Diagnostico");

            modelBuilder.Entity<Diagnostico>().Property(P => P.valoracionespecialista).IsRequired(false);
            modelBuilder.Entity<Diagnostico>().Property(P => P.enfermedad).IsRequired(false);

            ///relacion de diagnostico con cita id
            modelBuilder.Entity<Diagnostico>()
              .HasOne(d => d.cita)
              .WithOne(c => c.diagnostico);
        }

        private void ClaseCita(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cita>().ToTable("Cita");

            modelBuilder.Entity<Cita>().Property(P => P.motivocita).IsRequired(false);
        }

        private void ClaseMedico(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Medico>().ToTable("Medico");

            modelBuilder.Entity<Medico>().Property(P => P.Nombre).IsRequired(false);
            modelBuilder.Entity<Medico>().Property(P => P.Apellidos).IsRequired(false);
            modelBuilder.Entity<Medico>().Property(P => P.usuario).IsRequired(false);
            modelBuilder.Entity<Medico>().Property(P => P.Clave).IsRequired(false);
            modelBuilder.Entity<Medico>().Property(P => P.Email).IsRequired(false);
            modelBuilder.Entity<Medico>().Property(P => P.Password).IsRequired(false);
            modelBuilder.Entity<Medico>().Property(P => P.numcolegiado).IsRequired(false);
        }

        private void ClasePaciente(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Paciente>().ToTable("Paciente");
            //Colocar los Varlores a nulos
            modelBuilder.Entity<Paciente>().Property(P => P.Nombre).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.Apellidos).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.usuario).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.Clave).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.Email).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.Password).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.Nss).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.NumTarjeta).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.Telefono).IsRequired(false);
            modelBuilder.Entity<Paciente>().Property(P => P.Direccion).IsRequired(false);


            // Relación muchos a muchos entre Paciente y Medico








        }
    }
}